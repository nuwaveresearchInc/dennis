


A
Auto
Manual
{if(infCBRentryDOR.IO_boDIclsSTS,"Close","")}{if(infCBRentryDOR.IO_boDIopnSTS,"Open","")}


Open
Close
Entry Door

A
Auto
Manual
{if(infCBRexitDOR.IO_boDIclsSTS,"Close","")}{if(infCBRexitDOR.IO_boDIopnSTS,"Open","")}


Open
Close
Exit Door

A
Auto
Manual
{if(infTRAYexit.IO_boDIretPOS,"Retract","")}{if(infTRAYexit.IO_boDIextPOS,"Extend","")}




Extend
Retract
Exit Cylinder

Auto
Manual
{if(infCBRprgvlv.IO_boDOopn,"Open","Close")}
Open
Close
Purge Valve
A


Extend
Retract
Auto
Manual

Jog Ext. 
Jog Ret.

Location (mm)
Extend SP (mm)
Retract SP (mm)


Set Location 

As EXT SP
Set Location

As Ret SP
Push Cylinder
Purge SP (kPa)
Vacuum SP (kPa)
A


Pump Status
Infeed Chamber Vacuum Pump
{if(infCBRvacPmp.boDIaux,"ON", "OFF")}
A
Auto
Manual



A
Auto
Manual
{if(infCBRentryDOR.IO_boDIclsSTS,"Close","")}{if(infCBRentryDOR.IO_boDIopnSTS,"Open","")}


Infeed Belt
Start
Stop


A
Auto
Manual
{if(infCBRvacvlv.IO_boDOopn,"Open","Close")}
Open
Close
Vacuum Valve
