


{if(boGINFCBRFull,"Full","Empty")}
PSR: {frntPSR_inSCL} kPa
Total: {inGtrayCNTR}
Speed








Vacuum Valve
Purge Valve










Vacuum Valve
Purge Valve




{if(infPUSHcyl.CTL_boINextPOS,"Extended")}

{if(infPUSHcyl.CTL_boINretPOS,"Retracted")}

A
Auto
Manual
{if(infCBRentryDOR.IO_boDIclsSTS,"Close","")}

{if(infCBRentryDOR.IO_boDIopnSTS,"Open","")}
Open
Close
Entry Door

A
Auto
Manual
{if(outfCBRentryDOR.IO_boDIclsSTS,"Close","")}

{if(outfCBRentryDOR.IO_boDIopnSTS,"Open","")}
Open
Close
Entry Door

A
Auto
Manual
{if(outfCBRexitDOR.IO_boDIclsSTS,"Close","")}

{if(outfCBRexitDOR.IO_boDIopnSTS,"Open","")}
Open
Close
Exit Door

A
Auto
Manual
{if(infCBRexitDOR.IO_boDIclsSTS,"Close","")}

{if(infCBRexitDOR.IO_boDIopnSTS,"Open","")}
Open
Close
Exit Door

A
Auto
Manual
{if(infTRAYexit.IO_boDIretPOS,"Retract","")}

{if(infTRAYexit.IO_boDIextPOS,"Extend","")}


Extend
Retract
Exit Cylinder

A
Auto
Manual
{if(outfTRAYins.IO_boDIretPOS,"Retract","")}

{if(outfTRAYins.IO_boDIextPOS,"Extend","")}


Extend
Retract
Insert Cylinder

A
Auto
Manual
{if(outfTRAYexit.IO_boDIretPOS,"Retract","")}

{if(outfTRAYexit.IO_boDIextPOS,"Extend","")}


Extend
Retract
Exit Cylinder









{if(infCBRentryDOR.IO_boDIopnSTS,"Open","")}

{if(infCBRentryDOR.IO_boDIclsSTS,"Close","")}


{if(infCBRexitDOR.IO_boDIopnSTS,"Open","")}

{if(infCBRexitDOR.IO_boDIclsSTS,"Close","")}


{if(outfCBRexitDOR.IO_boDIopnSTS,"Open","")}

{if(outfCBRexitDOR.IO_boDIclsSTS,"Close","")}


{if(outfCBRentryDOR.IO_boDIopnSTS,"Open","")}

{if(outfCBRentryDOR.IO_boDIclsSTS,"Close","")}


{if(boGoutfCBRFull,"Full","Empty")}

psr: ### kPa

psr: ### kPa
PSR: {rearPSR_inSCL} kPa


Pump Status
Infeed Chamber Vacuum Pump
{if(infCBRvacPmp.boDIaux,"ON", "OFF")}
A
Auto
Manual




Pump Status
Outfeed Chamber Vacuum Pump
{if(outfCBRvacPmp.boDIaux,"ON", "OFF")}
A
Auto
Manual


{if(outfPULLcyl.CTL_boINextPOS,"Extended")}

{if(outfPULLcyl.CTL_boINretPOS,"Retracted")}


A
Extend
Retract
Auto
Manual
Jog Ext. 
Jog Ret.




Location (mm)
Error SP (+/- mm)


Extend SP (mm)
Retract SP (mm)


Set Location 

As EXT SP
Set Location

As Ret SP
Push Cylinder





 Command 
Auto
Manual
Bypass Encoder
Auto SPD (mm/s)
##.#
Man SPD (mm/s)
##.#
Bypass Encoder

A
Auto
Manual
{if(infCBRprgvlv.IO_boDOopn,"Open","Close")}
Open
Close
Purge Valve

A
Auto
Manual
{if(infCBRvacvlv.IO_boDOopn,"Open","Close")}
Open
Close
Vacuum Valve

A
Auto
Manual
{if(MagHeatExVlv.IO_boDOopn,"Open","Close")}
Open
Close
Heat Exchange Valve
