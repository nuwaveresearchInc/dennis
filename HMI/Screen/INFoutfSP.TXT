



Infeed Tray Req. Warning (min)
Infeed Tray Req. Alarm SP (min)
Belt Tooth Detect SP (min)
{if(Conv.boHautoman,"Auto Speed","Manual Speed")}  (mm/s)
Setpoints

{Format("%0."+Trunc(#Decimal:2)+"f",#Min:0)} {#Unit:"mm/s"}
{Format("%0."+Trunc(#Decimal:2)+"f",#Max:20)} {#Unit:"mm/s"}
{Format("%0."+Trunc(#Decimal:2)+"f",(#Max:20-#Min:0)/2+#Min:0)} {#Unit:"mm/s"}
{Format("%0."+Trunc(#Decimal:2)+"f",#Value:Conv.reSPDfbk)} {#Unit:"mm/s"}
Counts
{Conv.ulPLScnt}
{Conv.ulmm}
mm
Encoder Reset


Bypass Encoder
Conveyor Belt
Auto
Manual


Encoder

Belt Waiting
Belt Pause for Outfeed Cycle SP (%)

Belt Tooth Sensor:

Manual
{if(infCBRMainCbrVLV.IO_boDOopn,"Open","Close")}
Open
Close
Inf To Main Valve

Auto
Manual
{if(infCBRBallastVacVLV.IO_boDOopn,"Open","Close")}
Open
Close
Inf Ballast Tank Valve

Auto
Manual
{if(outfCBRMainCbrVLV.IO_boDOopn,"Open","Close")}
Open
Close
Outf To Main Valve

Auto
Manual
{if(outfCBRBallastVacVLV.IO_boDOopn,"Open","Close")}
Open
Close
Outf Ballast Tank Valve
Auto
Tracking
Write To PLC
Start Mag Teeth Cnt
Stop Mag Teeth Cnt

Pause

Psr




Inf
Outf
Chiller
HMI



