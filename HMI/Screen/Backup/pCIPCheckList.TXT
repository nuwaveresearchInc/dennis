
Close
{if( CIPFillVLV.boHAutoMan," CIP Fill Valve in Auto", "CIP Fill Valve in Manual")}
Auto Mode Requirements
{if( CIPdrainVLV.boHAutoMan," CIP Drain Valve in Auto", "CIP Drain Valve in Manual")}
{if( CIPreturnVLV.boHAutoMan," CIP Return Valve in Auto", "CIP Return Valve in Manual")}
{//if( CIPsprayVLV.boHAutoMan," CIP Spray Valve in Auto", "CIP Spray Valve in Manual")}
{if( CIPmtr.boHAutoMan," CIP Motor in Auto", "CIP Motor in Manual")}
{if( boGstmAUTOrun,"System Drying", "System Stopped")}
{if( EduButtVlv.boHAutoMan," Eductor Butt. Valve in Auto", "Eductor Butt. Valve in Manual")}
